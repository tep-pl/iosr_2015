Given /^"([^"]*)" is a user with social_id "([^"]*)" and password "([^"]*)"$/ do |full_name, social_id, password|
  first_name, last_name = full_name.split
  token = "123456789"
  @user = User.create(first_name: first_name, last_name: last_name, password: password, social_id: social_id, authentication_token: token)
  @user.save
end

And "his spreadsheets are" do |spreadsheet_data|
  spreadsheet_hashes = spreadsheet_data.hashes
  spreadsheet_hashes.each do |spreadsheet_hash|

    spreadsheet = Spreadsheet.new
    spreadsheet.user = @user
    spreadsheet.title = spreadsheet_hash["title"]
    spreadsheet.cellsDictionary = {}
    spreadsheet.shared = spreadsheet_hash["shared"]
    spreadsheet.save

  end
end

And "I am authenticated as him with his token header" do
  page.driver.header('X-auth-token', @user.authentication_token)
end

And "I am authenticated as anonymous user" do
  anonymousFirstName = "Anonymous"
  anonymousLastName = "User"
  anonymoysPassword = "testPass"
  anonymousSocial = "0101010101"
  anonymousToken = "AnonymousUserAuthToken"
  anonymous = User.create(first_name: anonymousFirstName, last_name: anonymousLastName, password: anonymoysPassword, social_id: anonymousSocial, authentication_token: anonymousToken)
  anonymous.save
  page.driver.header('X-auth-token', anonymous.authentication_token)
end

And 'his spreadsheet is' do |spreadsheet_data|
  spreadsheet_hashes = spreadsheet_data.hashes
  spreadsheet_hash = spreadsheet_hashes.first

  @spreadsheet = Spreadsheet.new
  @spreadsheet.user = @user
  @spreadsheet.title = spreadsheet_hash['title']
  @spreadsheet.cellsDictionary = {}
  @spreadsheet.shared = spreadsheet_hash['shared']
  @spreadsheet.save
end

And 'his charts are' do |charts_data|
  charts_hashes = charts_data.hashes

  charts_hashes.each do |chart_hash|
    chart = SpreadsheetChart.new
    chart.spreadsheet = @spreadsheet
    chart.color = chart_hash['color']
    chart.x = chart_hash['x']
    chart.y = chart_hash['y']
    chart.chartType = chart_hash['chartType']
    chart.save
  end
end
