Feature: Charts API

    Background:
      Given I send and accept JSON

    Scenario: Successfully list charts
      Given "Saul Hudson" is a user with social_id "123456789" and password "test12345"
      And his spreadsheet is
        |id|title   |shared|
        |1|chart_s |false |
      And I am authenticated as him with his token header
      And his charts are
        |id|color   |x |y   |chartType|
        |1|#0000CD |40|-660|Line     | 
        |2|#FFD700 |40|-660|Bar      |
        |3|#ADFF2F |40|-660|Bar      |
        |4|#000000 |40|-660|Line     |
      When I send a GET request to "/api/spreadsheets/1/charts"
      Then the response status should be "200"
      And the JSON response should be:
      """
      {
        "charts": [
          {
            "_id": 1,
            "chartType":"Line",
            "color":"#0000CD",
            "x":40,
            "xEnd": null,
            "xStart": null,
            "y": -660,     
            "yEnd": null,  
            "yStart": null 
          },
          {
            "_id": 2,
            "chartType":"Bar",
            "color":"#FFD700",
            "x":40,
            "xEnd": null,
            "xStart": null,
            "y": -660,     
            "yEnd": null,  
            "yStart": null 
          },
          {
            "_id": 3,
            "chartType":"Bar",
            "color":"#ADFF2F",
            "x":40,
            "xEnd": null,
            "xStart": null,
            "y": -660,     
            "yEnd": null,  
            "yStart": null 
          },
          {
            "_id": 4,
            "chartType":"Line",
            "color":"#000000",
            "x":40,
            "xEnd": null,
            "xStart": null,
            "y": -660,     
            "yEnd": null,  
            "yStart": null 
          }
        ]
      }
      """

    Scenario: Successfully create chart
      Given "Saul Hudson" is a user with social_id "123456789" and password "test12345"
      And his spreadsheet is
        |id|title   |shared|
        |1|chart_s |false |
      And I am authenticated as him with his token header
      When I send a POST request to "/api/spreadsheets/1/charts" with the following:
      """
      {
        "chartType":"Line",
        "color":"#000000",
        "x":40,
        "y":-660
      }
      """
      Then the response status should be "201"
      When I send a GET request to "/api/spreadsheets/1/charts"
      Then the response status should be "200"
      And the JSON response should be:
      """
      {
        "charts": [
          {
            "_id": 1,
            "chartType":"Line",
            "color":"#000000",
            "x":40,
            "xEnd": null,  
            "xStart": null,
            "y": -660,     
            "yEnd": null,  
            "yStart": null 
          }
        ]
      }
      """

    Scenario: Successfully create chart
      Given "Saul Hudson" is a user with social_id "123456789" and password "test12345"
      And his spreadsheet is
        |id|title   |shared|
        |1|chart_s |false |
      And I am authenticated as him with his token header
      When I send a POST request to "/api/spreadsheets/1/charts" with the following:
      """
      {
        "chartType":"Line",
        "color":"#000000",
        "x":40,
        "y":-660
      }
      """
      Then the response status should be "201"
      When I send a GET request to "/api/spreadsheets/1/charts"
      Then the response status should be "200"
      And the JSON response should be:
      """
      {
        "charts": [
          {
            "_id": 1,
            "chartType":"Line",
            "color":"#000000",
            "x":40,
            "xEnd": null,  
            "xStart": null,
            "y": -660,     
            "yEnd": null,  
            "yStart": null 
          }
        ]
      }
      """

    Scenario: Successfully update chart
      Given "Saul Hudson" is a user with social_id "123456789" and password "test12345"
      And his spreadsheet is
        |id|title   |shared|
        |1|chart_s |false |
      And I am authenticated as him with his token header
      And his charts are
        |id|color   |x |y   |chartType|
        |1 |#0000CD |40|-660|Line     | 
        |2 |#FFD700 |40|-660|Bar      |
      When I send a PUT request to "/api/spreadsheets/1/charts/1" with the following:
      """
      {
        "chartType":"Line",
        "color":"#000000",
        "x":40,
        "y":-660
      }
      """
      Then the response status should be "200"
      When I send a GET request to "/api/spreadsheets/1/charts/1"
      Then the response status should be "200"
      And the JSON response should be:
      """
      {
        "_id": 1,
        "chartType":"Line",
        "color":"#000000",
        "x":40,
        "xEnd": null,  
        "xStart": null,
        "y": -660,     
        "yEnd": null,  
        "yStart": null 
      }
      """
      When I send a PUT request to "/api/spreadsheets/1/charts/1" with the following:
      """
      {
        "chartType":"Bar",
        "color":"#FFD700",
        "x":40,
        "y": -660
      }
      """
      Then the response status should be "200"
      When I send a GET request to "/api/spreadsheets/1/charts/2"
      Then the response status should be "200"
      And the JSON response should be:
      """
      {
        "_id": 2,
        "chartType":"Bar",
        "color":"#FFD700",
        "x":40,
        "xEnd": null,  
        "xStart": null,
        "y": -660,     
        "yEnd": null,  
        "yStart": null 
      }
      """

    Scenario: Successfully delete a chart
      Given "Saul Hudson" is a user with social_id "123456789" and password "test12345"
      And his spreadsheet is
        |id|title   |shared|
        |1|chart_s |false |
      And I am authenticated as him with his token header
      And his charts are
        |id|color   |x |y   |chartType|
        |1 |#0000CD |40|-660|Line     | 
        |2 |#FFD700 |40|-660|Bar      |
      When I send a DELETE request to "/api/spreadsheets/1/charts/2" with the following:
      Then the response status should be "204"
      When I send a GET request to "/api/spreadsheets/1/charts"
      Then the response status should be "200"
      And the JSON response should be:
      """
      {
        "charts": [
          {
            "_id": 1,
            "chartType":"Line",
            "color":"#0000CD",
            "x":40,
            "xEnd": null,  
            "xStart": null,
            "y": -660,     
            "yEnd": null,  
            "yStart": null 
          }
        ]
      }
      """

    Scenario: Can't show chart which doesn't exist
      Given "Saul Hudson" is a user with social_id "123456789" and password "test12345"
      And his spreadsheet is
        |id|title   |shared|
        |1|chart_s |false |
      And I am authenticated as him with his token header
      And his charts are
        |id|color   |x |y   |chartType|
        |1 |#0000CD |40|-660|Line     | 
        |2 |#FFD700 |40|-660|Bar      |
      When I send a GET request to "/api/spreadsheets/1/charts/312"
      Then the response status should be "404"
