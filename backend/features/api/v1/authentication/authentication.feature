Feature: Sign Up

    Background:
      Given I send and accept JSON

    Scenario: Successful sign up
      When I send a POST request to "/api/users" with the following:
      """
      {
        "first_name": "Saul",
        "last_name": "Hudson",
        "social_id": "123456789",
        "password": "test12345"
      }
      """
      Then the response status should be "201"
      And the JSON response should have "token"
      And the JSON response at "token" should be a string
      

    Scenario: I successffully sign in 
      Given "Saul Hudson" is a user with social_id "123456789" and password "test12345"
      When I send a POST request to "/api/users" with the following:
      """
      {
        "first_name": "Saul",
        "last_name": "Hudson",
        "social_id": "123456789",
        "password": "test12345"
      }
      """
      Then the response status should be "200"
      And the JSON response should have "token"
      And the JSON response at "token" should be a string

    Scenario: Password is wrong
      Given "Saul Hudson" is a user with social_id "123456789" and password "test12345"
      When I send a POST request to "/api/users" with the following:
      """
      {
        "first_name": "Saul",
        "last_name": "Hudson",
        "social_id": "123456789",
        "password": "test123456789"
      }
      """
      Then the response status should be "403"
      And the JSON response should have "cause"
      And the JSON response at "cause" should be a string
      And the JSON response should be:
      """
      {"cause": "User found but wrong credentials supplied"}
      """