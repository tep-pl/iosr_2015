
Feature: Spreadsheets API

    Background:
      Given I send and accept JSON

    Scenario: Successfully list spreadsheets
      Given "Saul Hudson" is a user with social_id "123456789" and password "test12345"
      And his spreadsheets are
        |id|title |shared|
        |10|test1 |false |
        |11|test2 |false |
        |12|test3 |false |
        |13|test4 |false |
        |14|test5 |false |
      And I am authenticated as him with his token header
      When I send a GET request to "/api/spreadsheets"
      Then the response status should be "200"
      And the JSON response should be:
      """
      {
        "spreadsheets": [
          {
            "title":"test1"
          },
          {
            "title":"test2"
          },
          {
            "title":"test3"
          },
          {
           "title":"test4"
          },
          {
            "title":"test5"
          }
        ]
      }
      """

    Scenario: Successfully create spreadsheet
      Given "Saul Hudson" is a user with social_id "123456789" and password "test12345"
      And his spreadsheets are
        |id|title |shared|
        |10|test1 |false |
        |11|test2 |false |
        |12|test3 |false |
        |13|test4 |false |
        |14|test5 |false |
      And I am authenticated as him with his token header
      When I send a POST request to "/api/spreadsheets" with the following:
      """
      {
        "title":"test6"
      }
      """
      Then the response status should be "201"
      When I send a GET request to "/api/spreadsheets"
      Then the response status should be "200"
      And the JSON response should be:
      """
      {
        "spreadsheets": [
          {
            "title":"test1"
          },
          {
            "title":"test2"
          },
          {
            "title":"test3"
          },
          {
           "title":"test4"
          },
          {
            "title":"test5"
          },
          {
            "title":"test6"
          }
        ]
      }
      """
      
     Scenario: Successfully edit spreadsheet's data
      Given "Saul Hudson" is a user with social_id "123456789" and password "test12345"
      And his spreadsheets are
        |id|title |shared|
        |1 |test1 |false |
        |2 |test2 |false |
        |3 |test3 |false |
        |4 |test4 |false |
        |5 |test5 |false |
      And I am authenticated as him with his token header
      When I send a PUT request to "/api/spreadsheets/3" with the following:
      """
      {
        "id":"3",
        "title":"testTEST",
        "cellsDictionary": {},
        "shared": "true"
      }
      """
      Then the response status should be "200"
      And the JSON response should be:
      """
      {
        "_id": 3,
        "cellsDictionary": {},
        "shared": true,
        "title": "testTEST"
      }
      """

     Scenario: Successfully destroy own spreadsheet
      Given "Saul Hudson" is a user with social_id "123456789" and password "test12345"
      And his spreadsheets are
        |id|title |shared|
        |1 |test1 |false |
        |2 |test2 |false |
        |3 |test3 |false |
        |4 |test4 |false |
        |5 |test5 |false |
      And I am authenticated as him with his token header
      When I send a DELETE request to "/api/spreadsheets/3" with the following:
      """
      {
        "id":"3"
      }
      """
      Then the response status should be "204"
      When I send a GET request to "/api/spreadsheets"
      Then the response status should be "200"
      And the JSON response should be:
      """
      {
        "spreadsheets": [
          {
            "title":"test1"
          },
          {
            "title":"test2"
          },
          {
           "title":"test4"
          },
          {
            "title":"test5"
          }
        ]
      }
      """

     Scenario: Successfully show own spreadsheet
      Given "Saul Hudson" is a user with social_id "123456789" and password "test12345"
      And his spreadsheets are
        |id|title |shared|
        |1 |test1 |false |
        |2 |test2 |false |
        |3 |test3 |false |
        |4 |test4 |false |
        |5 |test5 |false |
      And I am authenticated as him with his token header
      When I send a GET request to "/api/spreadsheets/2"
      Then the response status should be "200"
      And the JSON response should be:
      """
      {
        "cellsDictionary": {},
        "shared": false,
        "title": "test2"
      }
      """

     Scenario: Can't show spreadsheet which doesn't exists
      Given "Saul Hudson" is a user with social_id "123456789" and password "test12345"
      And his spreadsheets are
        |id|title |shared|
        |1 |test1 |false |
        |2 |test2 |false |
        |3 |test3 |false |
        |4 |test4 |false |
        |5 |test5 |false |
      And I am authenticated as him with his token header
      When I send a GET request to "/api/spreadsheets/10"
      Then the response status should be "404"

     Scenario: Can't see someone else's spreadsheet which IS NOT public
      Given "Saul Hudson" is a user with social_id "123456789" and password "test12345"
      And his spreadsheets are
        |id|title |shared|
        |1 |test1 |false |
        |2 |test2 |false |
        |3 |test3 |false |
        |4 |test4 |false |
        |5 |test5 |false |
      And I am authenticated as anonymous user
      When I send a GET request to "/api/spreadsheets/2"
      Then the response status should be "403"

     Scenario: Can see someone else's spreadsheet which IS public
      Given "Saul Hudson" is a user with social_id "123456789" and password "test12345"
      And his spreadsheets are
        |id|title |shared|
        |1 |test1 |false |
        |2 |test2 |true  |
        |3 |test3 |false |
        |4 |test4 |false |
        |5 |test5 |false |
      And I am authenticated as anonymous user
      When I send a GET request to "/api/spreadsheets/2"
      Then the response status should be "200"
      And the JSON response should be:
      """
      {
        "cellsDictionary": {},
        "shared": true,
        "title": "test2"
      }
      """
