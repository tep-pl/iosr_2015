WebsocketRails::EventMap.describe do
  
  subscribe :spreadsheet_update, :to => WSController, :with_method => :update
  subscribe :spreadsheet_heartbeat, :to => WSController, :with_method => :heartbeat

end
