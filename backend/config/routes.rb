require 'api_constraints'

Rails4Api::Application.routes.draw do

  scope module: :v1, constraints: ApiConstraints.new(version: 1, default: :true) do
    devise_for :users, path: '/api/users', skip: [:sessions, :registrations]
  end

  devise_scope :user do
      post 'api/users', :to => 'api/v1/custom_devise/registrations#create', :as => 'user_registration'
  end

  namespace :api, defaults: {format: 'json'} do
    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: :true) do
      resources :spreadsheets, :only => [:index, :create, :show, :update, :destroy]
      resources :spreadsheets do
        post 'charts', to: 'charts#create'
        get 'charts', to: 'charts#index'
        delete 'charts/:id', to: 'charts#destroy'
        patch 'charts/:id', to: 'charts#update'
        put 'charts/:id', to: 'charts#update'
        get 'charts/:id', to: 'charts#show'
        get 'values/:id', to: 'spreadsheets#showComputedValue'
      end
    end
  end

  #root :to => "home#index"
end
