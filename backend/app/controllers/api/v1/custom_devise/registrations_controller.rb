module Api
  module V1
    module CustomDevise
      class RegistrationsController < Devise::RegistrationsController
        prepend_before_filter :require_no_authentication, :only => [ :create ]

        respond_to :json

        # POST /resource
        def create
          build_resource(auth_params)

          user = User.find_by(social_id: auth_params[:social_id])
          if user != nil
            if user.valid_password?(auth_params[:password])
              if resource.active_for_authentication?
                resource.reset_authentication_token
                user.update_attributes(authentication_token: resource.authentication_token)

                # broadcast new token message both to exisiting users and new one
                resultPayload = Hash.new
                resultPayload[:user] = user.social_id
                resultPayload[:token] = user.authentication_token
                WebsocketRails[:users].trigger 'refreshed_token', resultPayload

                render json: { token: user.authentication_token }, status: :ok
              else
                render json: { cause: "User's account inactive" }, status: :forbidden
              end
            else
              render json: { cause: "User found but wrong credentials supplied" }, status: :forbidden
            end
          else
            resource.reset_authentication_token
            if resource.save
              if resource.active_for_authentication?
                sign_up(resource_name, resource)
                render json: { token: resource.authentication_token }, status: :created
              else
                render json: { cause: "User's account inactive" }, status: :forbidden
              end
            else
              clean_up_passwords resource
              render json: { cause: resource.errors.full_messages }, status: :unprocessable_entity
            end
          end
        end

        private
          def auth_params
            params.permit(:first_name, :last_name, :social_id, :password)
          end
      end
    end
  end
end
