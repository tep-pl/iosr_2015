class Api::V1::ChartsController < ApplicationController
  respond_to :json

  def index
    spreadsheet_id = params[:spreadsheet_id]
    current_spreadsheet = current_user.spreadsheets.where(id: spreadsheet_id.to_i).one
    if current_spreadsheet.nil?
      head :no_content 
    else
      charts = current_spreadsheet.spreadsheetCharts
      render :json => charts.map { |s| s.as_json } , status: :ok
    end
  end

  def create
    chart = SpreadsheetChart.new
    spreadsheet_id = params[:spreadsheet_id]
    current_spreadsheet = current_user.spreadsheets.where(id: spreadsheet_id.to_i).one
    chart.spreadsheet = current_spreadsheet
    chart.x = params[:x]
    chart.y = params[:y]
    chart.color = params[:color]
    chart.chartType = params[:chartType]
    chart.xStart = params[:xStart]
    chart.xEnd = params[:xEnd]
    chart.yStart = params[:yStart]
    chart.yEnd = params[:yEnd]
    chart.save!
  	render :json => chart, status: :created
  end

  def show
    spreadsheet_id = params[:spreadsheet_id]
    current_spreadsheet = current_user.spreadsheets.where(id: spreadsheet_id.to_i).one
    chart = current_spreadsheet.spreadsheetCharts.where(id: params[:id].to_i).one
    if chart
  	  render :json => chart.as_json, status: :ok
    else
      head :not_found
    end
  end

  def update
    spreadsheet_id = params[:spreadsheet_id]
    current_spreadsheet = current_user.spreadsheets.where(id: spreadsheet_id.to_i).one
    chart = current_spreadsheet.spreadsheetCharts.where(id: params[:id].to_i).one
    chart.x = params[:x] if params[:x]
    chart.y = params[:y] if params[:y]
    chart.color = params[:color] if params[:color] 
    chart.chartType = params[:chartType] if params[:chartType] 
    chart.xStart = params[:xStart] if params[:xStart]
    chart.xEnd = params[:xEnd] if params[:xEnd]
    chart.yStart = params[:yStart] if params[:yStart]
    chart.yEnd = params[:yEnd] if params[:yEnd]
    charts = current_spreadsheet.spreadsheetCharts
    index = charts.index{ |ele| ele._id == params[:id].to_i }
    charts[index] == chart
    current_spreadsheet.update_attributes!
  	render :json => chart.as_json, status: :ok
  end

  def destroy
    spreadsheet_id = params[:spreadsheet_id]
    current_spreadsheet = current_user.spreadsheets.where(id: spreadsheet_id.to_i).one
    chart = current_spreadsheet.spreadsheetCharts.where(id: params[:id].to_i).one
    chart.destroy!
  	head :no_content
  end

  private
    def chart_params
      params.permit!
    end
end
