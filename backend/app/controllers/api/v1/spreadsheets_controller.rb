class Api::V1::SpreadsheetsController < ApplicationController
  respond_to :json

  def index
    spreadsheets = current_user.spreadsheets
    render :json => spreadsheets.map { |s| s.as_json(:only => [:id, :title]) } , status: :ok
  end

  def create
    spreadsheet = Spreadsheet.new
    spreadsheet.user = current_user
    spreadsheet.title = spreadsheet_params[:title]
    spreadsheet.activeCell = ''
    spreadsheet.cellsDictionary = {}
    spreadsheet.cellsValue = {}
    spreadsheet.shared = false
    spreadsheet.save!
    render :json => spreadsheet.as_json, status: :created
  end

  def show
    owner = nil
    spreadsheet = nil
    if spreadsheet_params[:id] =~ /\A[-+]?[0-9]+\z/
      # if we are looking for spreadsheet by its id
      owner = User.where('spreadsheets._id' => spreadsheet_params[:id].to_i).first
      if owner == nil
        head :not_found
      else
        spreadsheet = owner.spreadsheets.where(id: spreadsheet_params[:id].to_i).one
      end
    else
      # if we are looking for spreadsheet by its alias
      owner = User.where('spreadsheets.alias' => spreadsheet_params[:id]).first
      if owner == nil
        head :not_found
      else
        spreadsheet = owner.spreadsheets.where(alias: spreadsheet_params[:id]).one
      end
    end
    # by this point we can be sure that spreadsheet exists
    if owner.id == current_user.id or spreadsheet.shared
      render :json => spreadsheet.as_json(:only => [:id, :alias, :title, :cellsDictionary, :cellsValue, :shared, :cellsStyling]), status: :ok
    else
      head :forbidden
    end
  end

  def update
    # validate
    owner = User.where('spreadsheets.alias' => spreadsheet_params[:alias]).first
    if spreadsheet_params[:alias] and not owner == nil
      render :json => spreadsheet_params.as_json(:only => [:id, :alias]), status: 412 # Given alias already exists. Cannot continue processing.
    else
      owner = User.where('spreadsheets._id' => spreadsheet_params[:id].to_i).first
      spreadsheet = owner.spreadsheets.where(id: spreadsheet_params[:id].to_i).one
      # update some params
      spreadsheet.title = spreadsheet_params[:title]
      spreadsheet.cellsDictionary = spreadsheet_params[:cellsDictionary]
      spreadsheet.cellsValue = spreadsheet_params[:cellsValue]
      spreadsheet.shared = spreadsheet_params[:shared]
      spreadsheet.alias = spreadsheet_params[:alias]
      spreadsheet.activeCell = spreadsheet_params[:activeCell]
      spreadsheet.update_attributes!
      render :json => spreadsheet.as_json, status: :ok
    end
  end

  def destroy
    spreadsheet = current_user.spreadsheets.where(id: spreadsheet_params[:id].to_i).one
    spreadsheet.destroy!
    head :no_content
  end

  def showComputedValue
    spreadsheet_id = params[:spreadsheet_id]
    current_spreadsheet = current_user.spreadsheets.where(id: spreadsheet_id.to_i).one
    value_id = params[:id]
    value = current_spreadsheet.cellsValue[value_id]
    if value
      render json: { value: value }, status: :ok
    else
      head :not_found
    end
  end

  private
    def spreadsheet_params
      params.permit!
    end
end
