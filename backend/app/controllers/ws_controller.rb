require "soroban"

class WSController < WebsocketRails::BaseController


	def update

	    if message[:spreadsheet][:alias]
	      owner = User.where('spreadsheets.alias' => message[:spreadsheet][:alias]).first
	    else
	      owner = User.where('spreadsheets._id' => message[:spreadsheet][:id].to_i).first
	    end
	    spreadsheet = owner.spreadsheets.where(id: message[:spreadsheet][:id].to_i).one

		spreadsheet.cellsDictionary = message[:spreadsheet][:cellsDictionary]
		spreadsheet.activeCell = message[:spreadsheet][:activeCell]
		spreadsheet.cellsValue = message[:spreadsheet][:cellsValue]
		spreadsheet.cellsStyling = message[:spreadsheet][:cellsStyling]

		s = Soroban::Sheet.new()

		# key = spreadsheet.activeCell
		# value=spreadsheet.cellsDictionary[spreadsheet.activeCell]

		# begin
		# 	eval("s.#{key}=\"#{value}\"")
		# rescue Exception
		# 	puts "syntax error! at #{key}"
		# end

		# val=0
		# begin
		# 	eval("val = s.#{key}")
		# rescue Exception
		# 	val="ERR"
		# 	# puts "interpreter error! at #{key}"
		# end
	
		# puts "s.#{key}=#{val}"
		# spreadsheet.cellsValue[key]="#{val}"


		spreadsheet.cellsDictionary.each do |key, array|
			begin
				eval("s.#{key}=\"#{array}\"")
			rescue Exception
    			puts "syntax error! at #{key}"
    			next
  			end
		end
		spreadsheet.cellsDictionary.each do |key, array|

			begin
				val=0
				eval("val = s.#{key}")
			rescue Exception
				spreadsheet.cellsValue[key]="ERR"
    			# puts "interpreter error! at #{key}"
    			next
  			end
  			puts "s.#{key}=#{val}"
  			spreadsheet.cellsValue[key]="#{val}"
		end

		spreadsheet.update_attributes!

		resultPayload = Hash.new
		resultPayload[:source_id] = message[:source_id]
		resultPayload[:spreadsheet] = spreadsheet

		WebsocketRails[:spreadsheets].trigger 'new_data', resultPayload
	end

	def heartbeat 
		WebsocketRails[:spreadsheets].trigger 'heartbeat', message
	end
end