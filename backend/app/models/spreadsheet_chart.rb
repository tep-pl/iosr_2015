require 'autoinc'

class SpreadsheetChart
  include Mongoid::Document
  include Mongoid::Autoinc

  # model properties
  field :id,		type: Integer
  field :x,     type: Integer
  field :y,     type: Integer
  field :color, type: String
  field :chartType, type: String
  field :xStart, type: String
  field :xEnd, type: String
  field :yStart, type: String
  field :yEnd, type: String

  # id auto increment
  increments :id

  # relationships
  embedded_in :spreadsheet
end
