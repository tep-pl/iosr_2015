require 'autoinc'

class Spreadsheet
  include Mongoid::Document
  include Mongoid::Autoinc

  # model properties
  field :id,    type: Integer
  field :title,     type: String
  field :alias,   type: String
  field :activeCell,   type: String
  field :cellsDictionary, type: Hash
  field :cellsValue, type: Hash
  field :cellsStyling, type: Hash
  field :shared,  type: Boolean

  # id auto increment
  increments :id

  # relationships
  embedded_in :user
  embeds_many :spreadsheetCharts
end