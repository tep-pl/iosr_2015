require 'autoinc'

class User
  include Mongoid::Document
  include Mongoid::Autoinc
 
  # devise config (authentication)
  devise :database_authenticatable, :registerable, :token_authenticatable, :validatable

  # model properties
  field :id,                    type: Integer
  field :first_name,            type: String
  field :last_name,             type: String
  field :social_id,             type: String
  field :encrypted_password,    type: String
  field :authentication_token,	type: String

  # id auto increment
  increments :id

  # relationships
  embeds_many :spreadsheets

  def email_required?
    false
  end

  def email_changed?
    false
  end

  def as_json options={}
    {
      id: id,
      title: title
    }
  end
end
