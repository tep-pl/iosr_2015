'use strict'
/**
* 	Spreadsheets IOSR Components Module
**/
var coreModule = angular.module('coreModule', []);

// App component
coreModule.factory('Component', function()
{
    var Component = function(data) {
        //set defaults properties and functions
        angular.extend(this, {
            path                    :   "/",
            route                   :   {},
        });

        this.setComponent(data);

        console.log("Component created: " + this.path);
    };

    // Update component parameters
    Component.prototype.setComponent = function(params) {
        
        if(params != undefined)
            angular.extend(this, params);

        // Set additional component parameters if any needed
    };

    Component.prototype.getPath = function() {
    	return this.path;
    }

    Component.prototype.getRoute = function() {
    	return this.route;
    }

    return Component;
});

coreModule.service('globalService', function($rootScope) {

    var bindedSubscriptions = {};
    var dispatcher = null;

    return {

        initWsDispatcher    :   function () {
            dispatcher = new WebSocketRails($websocket_url);
            return dispatcher;
        },

        getRequestHeaders   :   function(token) {
            if(token == undefined) {
                token = localStorage.getItem("iosrApp-user-token");
            }
            return {
                "Access-Control-Allow-Headers"  :   "X-Auth-Token",
                "X-Auth-Token"                  :   token,
                "Accept"                        :   "application/json",
                "Content-Type"                  :   "application/json"
            };
        },

        getRandString       :   function(x) {
            var s = "";
            while(s.length<x&&x>0) {
                var r = Math.random();
                s += (r<0.1?Math.floor(r*100):String.fromCharCode(Math.floor(r*26) + (r>0.5?97:65)));
            }
            return s;
        },

        bindSubscription       :   function(subscription) {
            if(bindedSubscriptions[subscription.channel] == undefined) 
                bindedSubscriptions[subscription.channel] = [].concat(subscription.event);
            else
                bindedSubscriptions[subscription.channel] = bindedSubscriptions[subscription.channel].concat(subscription.event);
        },

        isSubscriptionBinded   :  function(subscription) {
            if(bindedSubscriptions[subscription.channel] == undefined) 
                return false;
            else if(bindedSubscriptions[subscription.channel].indexOf(subscription.event) > -1)
                return true;
            else
                return false;
        },

        clearSubscriptions      :  function() {
            for(name in bindedSubscriptions)
                dispatcher.unsubscribe(name);
            bindedSubscriptions = {};
        }
    }
});