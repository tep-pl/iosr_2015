'use strict'
/**
*   Spreadsheets IOSR Spreadsheets Module
**/
var spreadsheetsModule = angular.module('spreadsheetsModule', []);

/**
 * Models - Spreadsheets
 */
// Spreadsheet model
spreadsheetsModule.factory('Spreadsheet', function($http, $rootScope, $location, $timeout, globalService)
{
    var spreadsheets_service_endpoint = "api/spreadsheets"; // Spreadsheets endpoint

    // create our new custom object that reuse the original object constructor
    var Spreadsheet = function(data) {
        //set defaults properties and functions
        angular.extend(this, {
            id              :   "",
            title           :   "",
            alias           :   null,
            shared          :   false,
            cellsDictionary :   {},
            cellsValue :   {},
            cellsStyling: {}
        }); 

        this.update(data);

        console.log("Spreadsheet created: " + this.title);
    };

    // Update Spreadsheet data
    Spreadsheet.prototype.update = function(params) {
        if(params == undefined) return;

        angular.extend(this, params);

        // add additional parameters if any needed

        console.log("Spreadsheet data updated");
    };

    // Create new Spreadsheet
    Spreadsheet.prototype.create = function() {
        var self = this;
        
       $http({
            url     :   $rootScope.rest_url + spreadsheets_service_endpoint,
            method  :   'POST',
            headers :   globalService.getRequestHeaders($rootScope.user.getAuthToken()),
            data    :   this
        }).success(function(response, status, headers) {
            console.log('Spreadsheet submited');
            $location.path('spreadsheet/');
        });
    };

    // Save existing Spreadsheet changes
    Spreadsheet.prototype.save = function() {
        var self = this;

       $http({
            url     :   $rootScope.rest_url + spreadsheets_service_endpoint + "/" + self.id,
            method  :   'PATCH',
            headers :   globalService.getRequestHeaders($rootScope.user.getAuthToken()),
            data    :   this
        }).success(function(response, status, headers) {
            console.log('Spreadsheet changes submited');
        }).error(function(response, status, headers) {
            if(status == 412) {
                $location.path('spreadsheet/' + response.id + '/edit/');
                $timeout(function() {
                    angular.element('#form-notification').notify({
                        message: { text: 'Supplied alias: \"'+ response.alias +'\" already taken' },
                        type: 'danger'
                    }).show();
                }, 800);
            }
        });
    };

    // getters
    Spreadsheet.prototype.getTitle = function() {
        return this.title;
    }

    return Spreadsheet;
});

/*
* Directives
*/
// Delays model update to input commit moment
spreadsheetsModule.directive('ngModelOnblur', function() {
    return {
        restrict: 'A',
        require: 'ngModel',
        priority: 1, // needed for angular 1.2.x
        link: function(scope, elm, attr, ngModelCtrl) {
            if (attr.type === 'radio' || attr.type === 'checkbox') return;

            elm.unbind('input').unbind('keydown').unbind('change');
            elm.bind('blur', function() {
                scope.$apply(function() {
                    ngModelCtrl.$setViewValue(elm.val());
                });         
            });
        }
    };
});
spreadsheetsModule.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

/*
* Controllers
*/
// Spreadsheets general controller
spreadsheetsModule.controller('spreadsheetsCtrl', function($compile, $scope, $rootScope, $timeout, $http, $location, $interval, Spreadsheet, globalService) {
    var spreadsheets_service_endpoint = "api/spreadsheets";
    var spreadsheet_id = $rootScope.component.route.action1;
    var charts_service_endpoint = "api/spreadsheets/" + spreadsheet_id + "/charts";
    $scope.columns = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I'];
    $scope.rows = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
    $scope.linkAvailable = false;
    $scope.shareLink = "";
    $rootScope.color = {
        code: ""
    };

    function getXCellsData(xRange) {
      var start = xRange.split(':')[0]
      var end = xRange.split(':')[1]
      console.log("start " + start);
      console.log("end " + end);
    }

    function getCellsData(range) {
      var columnStart = range.split(':')[0].match(/[A-Z]/)[0];
      var columnEnd = range.split(':')[1].match(/[A-Z]/)[0];
      var rowStart = range.split(':')[0].match(/\d+/)[0];
      var rowEnd = range.split(':')[1].match(/\d+/)[0];
      var i = rowStart;
      var rangeArray = [];
      for( i = rowStart ; i <= rowEnd; i++ ) {
        var key = columnStart + i;
        if( $rootScope.spreadsheet.cellsValue[key] == undefined) {
          wrongDataError();
          return false;
        }
        rangeArray.push($rootScope.spreadsheet.cellsValue[key]);
      }
      return rangeArray;
    }

    function drawChart(chart) {
      var chart_id = spreadsheet_id + "-" + chart.id;
      if( document.getElementById(chart_id) != undefined ) return;

      var chartsSpace = document.getElementById('spreadsheet-table');
      var top = chart.y;
      var left = chart.x;
      console.log("Drawing chart x " + left + " y " + top);
      var compiledElement = '<canvas ng-controller="chartsController" ng-click="getCurrentChartId($event.currentTarget.id)" class="draggable" id="' + chart_id + '" width="400" height="400" style="border:1px solid #000000; left: ' + left +'px; top: ' + top + 'px;"></canvas>';
      angular.element(chartsSpace).append($compile(compiledElement)($scope));
      jQuery(function() {
        jQuery('.draggable').draggable( {
          preventCollision: true,
          containment: 'parent'
        });
      });
     
      var xRange = chart.xStart + ':' + chart.xEnd;
      var yRange = chart.yStart + ':' + chart.yEnd;
      var xData = getCellsData(xRange);
      var yData = getCellsData(yRange);

      var ele = document.getElementById(chart_id);
      var ctx = ele.getContext('2d');
      var data = {
            labels: xData,
            datasets: [
                {
                  fillColor: chart.color,
                  data: yData
                }
            ]
        };

      if (chart.chartType === 'Line') { 
        new Chart(ctx).Line(data);
      } else if (chart.chartType === 'Bar') {
        new Chart(ctx).Bar(data);
      }
    }

    function drawSpreadsheetCharts(charts) {
      var i = 0;
      for( i = 0; i < charts.length; i++) {
        drawChart(charts[i]);
      }
    }

    if(spreadsheet_id != undefined) {
        $http({
            url     :   $rootScope.rest_url + spreadsheets_service_endpoint + "/" + spreadsheet_id,
            method  :   'GET',
            headers :   globalService.getRequestHeaders($rootScope.user.getAuthToken()),
        }).success(function(response, status, headers) {
            
            // prepare spreadsheet
            var spreadsheet = new Spreadsheet(response);
            $rootScope.spreadsheet = spreadsheet;

            // prepare sharing link
            if(spreadsheet.shared) {
                var link = $location.protocol() + "://" + $location.host() + ":" + $location.port() + "/" + $rootScope.component.route.componentName + "/";
                link += spreadsheet.alias ? spreadsheet.alias + "/" : spreadsheet.id + "/";
                $scope.shareLink = link;
            }

            /*
            *   WebSockets Heartbeat
            */
            var collaborators = { }
            // new heartbeat message listening
            var subscriptionData = {
                channel     :    'spreadsheets',
                event       :    'heartbeat'
            };
            if(!globalService.isSubscriptionBinded(subscriptionData)) {
                globalService.bindSubscription(subscriptionData);
                
                $rootScope.wsDispatcher.subscribe(subscriptionData.channel).bind(subscriptionData.event, function(message) {
                    if(message.source_id == $rootScope.source_id)
                        return;

                    if($rootScope.spreadsheet.id != message.spreadsheet_id)
                        return;

                    collaborators[message.username] = Date.now();
                    $scope.users = Object.keys(collaborators);
                });
            }

            // heartbeat message payload
            var heartBeatPayload = { 
                username          :   $rootScope.user.firstName + ' ' + $rootScope.user.lastName, 
                source_id         :   $rootScope.source_id,
                spreadsheet_id    :   $scope.spreadsheet.id
            };
            // init intervals
            var heartBeat = $interval(function() {
                $rootScope.wsDispatcher.trigger('spreadsheet_heartbeat', heartBeatPayload);
            }, 15000);
            var cleaning = $interval(function() {
                var now = Date.now();
                for(var c in collaborators) {
                    if(now - collaborators[c] > 18000) {
                        delete collaborators[c];
                        $scope.users = Object.keys(collaborators);
                    }
                }
            }, 12000);
            // stop intervals when closing
            $scope.$on('$destroy', function() {
                if(angular.isDefined(heartBeat))
                    $interval.cancel(heartBeat);

                if(angular.isDefined(cleaning)) 
                    $interval.cancel(cleaning);
            });

            console.log("Spreadsheet #" + spreadsheet_id + " preview loaded");
        }).error(function(response, status, headers) {
            $rootScope.errorOccurred = true;
        });

        $http({
            url     :   $rootScope.rest_url + charts_service_endpoint,
            method  :   'GET',
            headers :   globalService.getRequestHeaders($rootScope.user.getAuthToken()),
        }).success(function(response, status, headers) {
            var i = 0;
            var charts = response.charts
            if (charts.length != 0) {
              console.log("Spreadsheet charts: ");
              console.log(charts);
              drawSpreadsheetCharts(charts);
            }
        }).error(function(response, status, headers) {
            $rootScope.errorOccurred = true;
        });

    } else {
        $http({
            url     :   $rootScope.rest_url + spreadsheets_service_endpoint,
            method  :   'GET',
            headers :   globalService.getRequestHeaders($rootScope.user.getAuthToken()),
        }).success(function(response, status, headers) {
            $scope.list = response;

            console.log("Spreadsheets list loaded");
        });
    }

    // when spreadsheet update received
    var subscriptionData = {
        channel     :    'spreadsheets',
        event       :    'new_data'
    };
    if(!globalService.isSubscriptionBinded(subscriptionData)) {
        globalService.bindSubscription(subscriptionData);
        
        $rootScope.wsDispatcher.subscribe(subscriptionData.channel).bind(subscriptionData.event, function(message) {
            if(message.source_id == $rootScope.source_id)
                return;

            if($rootScope.spreadsheet.id != message.spreadsheet.id)
                return;

            $rootScope.spreadsheet.update(message.spreadsheet);
            $rootScope.$apply();

            angular.element('#update-notification').notify({
                message: { text: 'Content edited by someone else' }
            }).show();
        });
    }

    // when cell was cliced
    $scope.setSelected = function() {

        var index = this.column+this.row
        $scope.spreadsheet.activeCell = index

        // FIXME FIXME FIXME FIXME

        // $http({
        //         url     :   $rootScope.rest_url + spreadsheets_service_endpoint + "/" + spreadsheet_id + "/values/" +index,
        //         method  :   'GET',
        //         headers :   globalService.getRequestHeaders($rootScope.user.getAuthToken()),
        //     }).success(function(response, status, headers) {

        //          var value = response.value
        //          $scope.spreadsheet.cellsValue[index] = value
              
        // });
        // var wsPayload = { 
        //     user            :   $rootScope.user.social_id, 
        //     source_id       :   $rootScope.source_id,
        //     spreadsheet     :   $scope.spreadsheet
        // };

        // // send update message
        // $rootScope.wsDispatcher.trigger('spreadsheet_update', wsPayload);


    }

    // when f(x) data changed
    $rootScope.updateCells = function() {
        // filter empty cells
        var cellsDictionary = $scope.spreadsheet.cellsDictionary
        for (var i in cellsDictionary) 
            if (cellsDictionary[i] === '') 
                delete cellsDictionary[i];

        var index = $scope.spreadsheet.activeCell

        // prepare message payload
        var wsPayload = { 
            user            :   $rootScope.user.social_id, 
            source_id       :   $rootScope.source_id,
            spreadsheet     :   $scope.spreadsheet
        };

        // send new f(x) data on selected cell
        $rootScope.wsDispatcher.trigger('spreadsheet_update', wsPayload);


        //get new f(x) function value
        // $http({

        //         url     :   $rootScope.rest_url + spreadsheets_service_endpoint + "/" + spreadsheet_id + "/values/" +index,
        //         method  :   'GET',
        //         headers :   globalService.getRequestHeaders($rootScope.user.getAuthToken()),
        //     }).success(function(response, status, headers) {

        //          var value = response.value
        //          $scope.spreadsheet.cellsValue[index] = value

        // });


        $http({
                url     :   $rootScope.rest_url + spreadsheets_service_endpoint + "/" + spreadsheet_id,
                method  :   'GET',
                headers :   globalService.getRequestHeaders($rootScope.user.getAuthToken()),
            }).success(function(response, status, headers) {
                var spreadsheet = new Spreadsheet(response);
                // var cellsValue = spreadsheet.cellsValue

                $scope.spreadsheet.cellsValue = spreadsheet.cellsValue
                

        });
        var wsPayload = { 
            user            :   $rootScope.user.social_id, 
            source_id       :   $rootScope.source_id,
            spreadsheet     :   $scope.spreadsheet
        };

        // send update message
        $rootScope.wsDispatcher.trigger('spreadsheet_update', wsPayload);

    }
    $scope.share = function() {
        // share current spreadsheet
        $rootScope.spreadsheet.shared = true;
        $rootScope.spreadsheet.save();
        var link = $location.protocol() + "://" + $location.host() + ":" + $location.port() + "/" + $rootScope.component.route.componentName + "/" + $rootScope.component.route.action1 + "/";
        $scope.shareLink = link;
    }

    $scope.showShareLink = function() {
        $scope.linkAvailable = true;
    }

    $scope.editSpreadsheet = function(id) {
        $location.path('spreadsheet/' + id + '/edit/')
    }

    $scope.getCurrentChartId = function(id) {
      $scope.currentChartId = id;
    };

    $rootScope.cellStyling = function(parameter, value, deletable) {
        var cell = $scope.spreadsheet.activeCell;
        var styling = $rootScope.spreadsheet.cellsStyling[cell];
        deletable = deletable === undefined ? true : false;
        if(styling)
            if(styling[parameter])
                if(deletable)
                    delete $rootScope.spreadsheet.cellsStyling[cell][parameter]
                else
                    $rootScope.spreadsheet.cellsStyling[cell][parameter] = value;
            else 
                $rootScope.spreadsheet.cellsStyling[cell][parameter] = value;
        else {
            $rootScope.spreadsheet.cellsStyling[cell] = { };
            $rootScope.spreadsheet.cellsStyling[cell][parameter] = value;
        }
        $rootScope.updateCells();
    }
});

// Spreadsheets create controller
spreadsheetsModule.controller('spreadsheetCreateCtrl', function($scope, Spreadsheet) {
    // Set scope model
    $scope.model = new Spreadsheet();

    // Setup form
    $scope.form = {
        submit              :   function(form) {
            var transport_object = {};
            angular.extend(transport_object, $scope.model);
            transport_object = new Spreadsheet(transport_object);
            transport_object.create();
        }
    };

    console.log("Spreadsheet-create form loaded");
});

// Spreadsheets create controller
spreadsheetsModule.controller('spreadsheetEditCtrl', function($scope, $rootScope, $http, $location, Spreadsheet, globalService) {
   var spreadsheets_service_endpoint = "api/spreadsheets/" + $rootScope.component.route.action1;

    $http({
        url     :   $rootScope.rest_url + spreadsheets_service_endpoint,
        method  :   'GET',
        headers :   globalService.getRequestHeaders($rootScope.user.getAuthToken()),
    }).success(function(response, status, headers) {

        //console.log(response);

        // Set scope model
        delete $rootScope.model;
        $scope.model = new Spreadsheet(response);
        $scope.model.alias = $scope.model.alias ? $scope.model.alias : $scope.model.id;
        // Setup form
        $scope.form = {
            submit              :   function(form) {
                var transport_object = {};
                angular.extend(transport_object, $scope.model);
                transport_object = new Spreadsheet(transport_object);
                transport_object.save();
                $location.path('spreadsheet/')
            }
        };

        console.log("Scene #" + $rootScope.component.route.action1 + " edit form loaded");
    });
});

// Spreadsheets delete controller
spreadsheetsModule.controller('spreadsheetRemoveCtrl', function($rootScope, $http, $location, globalService) {
   var spreadsheets_service_endpoint = "api/spreadsheets/" + $rootScope.component.route.action1;

    console.log('remove controller');

    $http({
        url     :   $rootScope.rest_url + spreadsheets_service_endpoint,
        method  :   'DELETE',
        headers :   globalService.getRequestHeaders($rootScope.user.getAuthToken()),
    }).success(function(response, status, headers) {
        console.log("Scene #" + $rootScope.component.route.action1 + " removed");
        $location.path('spreadsheet/')
    });
});
