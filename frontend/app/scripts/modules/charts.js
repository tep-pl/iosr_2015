'use strict';

var chartsModule = angular.module('chartsModule', []);

/*
* Controllers
*/

chartsModule.value('ChartId', { value : "0-0" } );
chartsModule.value('ChartIdUpdated', { value : "0-0" } );

// Chart model
chartsModule.factory('SpreadsheetChart', function($http, $rootScope, $location, globalService, ChartId, ChartIdUpdated)
{
  var spreadsheet_endpoint = "api/spreadsheets/";
  var charts_endpoint = "/charts/";

  function chartExists(id) {
    var matches = id.match(/\d+-\d+/);
    if (matches == null) return false;
    return true;
  }

  // create our new custom object that reuse the original object constructor
  var SpreadsheetChart = function(data) {
      //set defaults properties and functions
      angular.extend(this, {
          color : '',
          chartType: ''
      }); 

      this.update(data);

      console.log("SpreadsheetChart created for spreadsheet " + $rootScope.component.route.action1);
  };

  // Update Chart data
  SpreadsheetChart.prototype.update = function(params) {
      if(params == undefined) return;

      angular.extend(this, params);

      // add additional parameters if any needed

      console.log("SpreadsheetChart data updated");
      console.log(params);
  };

  // Create new Chart
  SpreadsheetChart.prototype.create = function() {
     var self = this;
     var method = 'POST';
     var url = $rootScope.rest_url + spreadsheet_endpoint + $rootScope.component.route.action1 + charts_endpoint;
     
     if(chartExists(ChartId.value)) {
       url = $rootScope.rest_url + spreadsheet_endpoint + $rootScope.component.route.action1 + charts_endpoint + ChartId.value.split("-").pop();
       method = 'PATCH'
     }

    console.log("URRRRL " + url);

     var promise = $http({
          url     :   url,
          method  :   method,
          headers :   globalService.getRequestHeaders($rootScope.user.getAuthToken()),
          data    :   this
      }).success(function(response, status, headers) {
          console.log('SpreadsheetChart submited for spreadsheet '+ $rootScope.component.route.action1);
          console.log(method + " --RESPONSE");
          console.log(response);
          url = $rootScope.rest_url + spreadsheet_endpoint + $rootScope.component.route.action1 + charts_endpoint + ChartId.value.split("-").pop();
          $http({
              url     :   url,
              method  :   'GET',
              headers :   globalService.getRequestHeaders($rootScope.user.getAuthToken()),
          }).success(function(response, status, headers) {
              var chart = response;
              console.log("GOT CHART AFTER --:" + method);
              console.log(chart);
          }).error(function(response, status, headers) {
              $rootScope.errorOccurred = true;
          });
      });
     return promise;
  };

  // Save existing SpreadsheetChart changes
  SpreadsheetChart.prototype.save = function() {
      var self = this;
      var url = $rootScope.rest_url + spreadsheet_endpoint + $rootScope.component.route.action1 + charts_endpoint + ChartId.value.split("-").pop();

     $http({
          url     :   url,
          method  :   'PATCH',
          headers :   globalService.getRequestHeaders($rootScope.user.getAuthToken()),
          data    :   this
      }).success(function(response, status, headers) {
          console.log("PATCH RESPONSE");
          console.log(response);
          $http({
              url     :   url,
              method  :   'GET',
              headers :   globalService.getRequestHeaders($rootScope.user.getAuthToken()),
          }).success(function(response, status, headers) {
              var chart = response;
              console.log("GOT CHART AFTER UPDATE:");
              console.log(chart);
          }).error(function(response, status, headers) {
              $rootScope.errorOccurred = true;
          });
          console.log('SpreadsheetChart changes submited');
      });
  };

    return SpreadsheetChart;
});

// Charts controller
chartsModule.controller('chartsController',  function($rootScope, $scope, $http, SpreadsheetChart, globalService, ChartId) {

  var colors = {
    blue : '#0000CD',
    yellow : '#FFD700',
    green : '#ADFF2F',
    black : '#000000'
  };

  $scope.chartsNumber = 0;
  $scope.fillColor = colors.blue;
  $scope.charType = 'Line';

  var charts = {};

  function chartExists(id) {
    var matches = id.match(/\d+-\d+/);
    if (matches == null) return false;
    return true;
  }

  $scope.removeChart = function() {
    if (!chartExists(ChartId.value)) return;
    var spreadsheet_endpoint = "api/spreadsheets/";
    var charts_endpoint = "/charts/";
    console.log('ChartId' + ChartId.value);
    var thisSpreadshitChartId = ChartId.value.split("-").pop();

    $http({
        url     :   $rootScope.rest_url + spreadsheet_endpoint + $rootScope.component.route.action1 + charts_endpoint + thisSpreadshitChartId,
        method  :   'DELETE',
        headers :   globalService.getRequestHeaders($rootScope.user.getAuthToken()),
    }).success(function(response, status, headers) {
        console.log("chart #" + ChartId.value + " removed");
    });
  }

  function updateChart() {
    if (!chartExists(ChartId.value)) return;
    var spreadsheet_endpoint = "api/spreadsheets/";
    var charts_endpoint = "/charts/";
    var thisSpreadshitChartId = ChartId.value.split("-").pop();
    $http({
        url     :   $rootScope.rest_url + spreadsheet_endpoint + $rootScope.component.route.action1 + charts_endpoint + thisSpreadshitChartId,
        method  :   'GET',
        headers :   globalService.getRequestHeaders($rootScope.user.getAuthToken()),
    }).success(function(response, status, headers) {
        var chart = response;
        console.log("GOT CHART:");
        chart = new SpreadsheetChart(chart);
        var numberPattern = /-?\d+/;
        var ele = document.getElementById(ChartId.value);
        var x = ele.style.left.match(numberPattern)[0];
        var y = ele.style.top.match(numberPattern)[0];
        chart.x = x;
        chart.y = y;
        chart.save();
        console.log(chart);
    }).error(function(response, status, headers) {
        $rootScope.errorOccurred = true;
    });
  }

  $scope.getCurrentChartId = function(id) {
    ChartId.value = id;
    jQuery(function() {
      jQuery('.draggable').draggable( {
        stop: function(event, ui) {
          console.log("DRAG STOPPED");
          preventCollision: true,
          updateChart();
        }
      });
    });
    console.log(ChartId.value);
  };

  $scope.chartAdded = function() {
    $scope.noChartAdded = false;
  };

  $scope.setChartType = function(barElement) {
    $scope.charType = barElement;
  };

  $scope.setChartColor = function(color) {
    if (color === 'blue') {
      $scope.fillColor = colors.blue;
    }
    else if (color === 'black') {
      $scope.fillColor = colors.black;
    }
    else if (color === 'yellow') {
      $scope.fillColor = colors.yellow;
    }
    else if (color === 'green') {
      $scope.fillColor = colors.green;
    }
  };

  function wrongDataError() {
    alert('wrong data') ;
  }

  function getCellsData(range) {
    var columnStart = range.split(':')[0].match(/[A-Z]/)[0];
    var columnEnd = range.split(':')[1].match(/[A-Z]/)[0];
    var rowStart = range.split(':')[0].match(/\d+/)[0];
    var rowEnd = range.split(':')[1].match(/\d+/)[0];
    var i = rowStart;
    var rangeArray = [];
    for( i = rowStart ; i <= rowEnd; i++ ) {
      var key = columnStart + i;
      if( $rootScope.spreadsheet.cellsValue[key] == undefined) {
        wrongDataError();
        return false;
      }
      rangeArray.push($rootScope.spreadsheet.cellsValue[key]);
    }
    return rangeArray;
  }

  function verifyRanges(xRange, yRange) {
    var rangePattern = /[A-Z]\d+:[A-Z]\d+/;
    var startX = xRange.split(':')[0].match(/[A-Z]/)[0];
    var endX = xRange.split(':')[1].match(/[A-Z]/)[0];
    var startY = yRange.split(':')[0].match(/[A-Z]/)[0];
    var endY = yRange.split(':')[1].match(/[A-Z]/)[0];
    var endEqualsStartX = startX == endX;
    var endEqualsStartY = startY == endY;
    if( !rangePattern.test(xRange) || !rangePattern.test(yRange) || 
        !endEqualsStartX || !endEqualsStartY ) {
          wrongDataError();
          return false;
    }
    return true;
  }

  $scope.drawChart = function() {
    var xRange = document.getElementById('xRange').value;
    var yRange = document.getElementById('yRange').value;

    if(! verifyRanges(xRange, yRange)) {
      return;
    }
    var rangeArrayX = getCellsData(xRange);
    var rangeArrayY = getCellsData(yRange);
    if( !rangeArrayX || !rangeArrayY) { 
      return;
    }
  
    var xData = rangeArrayX;
    var yData = rangeArrayY;

    $scope.charTypeNotSelected = false;
    $scope.noChartAdded = false;

    var ele = document.getElementById(ChartId.value);
    if (ele === null) {
      $scope.noChartAdded = true;
      return;
    }
    var ctx = ele.getContext('2d');
    var data = {
          labels: xData,
          datasets: [
              {
                fillColor: $scope.fillColor,
                data: yData
              }
          ]
      };

    if ($scope.charType === 'Bar') { 
      new Chart(ctx).Bar(data);
    } else {
      new Chart(ctx).Line(data);
    }

    var xStart = xRange.split(':')[0]
    var xEnd = xRange.split(':')[1]
    var yStart = yRange.split(':')[0]
    var yEnd = yRange.split(':')[1]

    var numberPattern = /-?\d+/;
    var data = {
      x : ele.style.left.match(numberPattern)[0],
      y : ele.style.top.match(numberPattern)[0],
      color : $scope.fillColor,
      chartType : $scope.charType,
      xStart : xStart,
      xEnd : xEnd,
      yStart : yStart,
      yEnd : yEnd 
    }
    $scope.model = new SpreadsheetChart(data);
    var promise = $scope.model.create();
    promise.then(function(response) {
      ChartId.value = $rootScope.component.route.action1 + "-" + response.data._id; 
      ele.id = ChartId.value;
    }, function(reason) {
      console.log('Failed: ' + reason);
    });
  };
});

chartsModule.directive('addChartsButton', function($compile) {
  return function(scope, element) {
    element.bind('click', function() {
      scope.chartsNumber++;
      scope.noChartAdded = false;
      var chartsSpace = document.getElementById('spreadsheet-table');
      var chartId = 'myChart' + scope.chartsNumber;
      var compiledElement = '<canvas ng-click="getCurrentChartId($event.currentTarget.id)" class="draggable" id="' + chartId + '" width="400" height="400" style="border:1px solid #000000; left: 40px; top: -600px;"></canvas>';
      angular.element(chartsSpace).append($compile(compiledElement)(scope));
      jQuery(function() {
        jQuery('.draggable').draggable( {
          containment: 'parent'
        });
      });
    });
  };
});

chartsModule.directive('removeChartsButton', function(ChartId) {
 return function(scope, element){
    var spreadsheet_endpoint = "api/spreadsheets/";
    var charts_endpoint = "/charts/";
    element.on('click', function () {
      var ele = document.getElementById(ChartId.value);
      ele.remove();
  });
  };
});
