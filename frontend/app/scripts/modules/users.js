'use strict'
/**
* 	Spreadsheets IOSR Users Module
**/
var usersModule = angular.module('usersModule', []);

/**
* Models
*/
// Single User Model
usersModule.factory('User', function($rootScope, $http, $location, globalService) {
	var ls_record_name = "iosrApp-user";
    var token_record_name = "iosrApp-user-token";
    var rest_services_url = $rootScope.rest_url;

    var general_service_endpoint = "api/users";

	var User = function(data) {
        angular.extend(this, {
            social_id       :   null, 
            firstName       :   null, 
            lastName        :   null,
            token           :   null,
            readyForAuth    :   false,
            anonymous       :   false
        });

        this.setUser(data);

        if(this.social_id != undefined) {
            console.log("User created: " + this.social_id);
        }
    };

    // Set User data
    User.prototype.setUser = function(data) {

        if(data != undefined)
           angular.extend(this, data);

        if(this.firstName == 'Anonymous' && this.lastName == 'User') 
           this.anonymous = true;
        else
           this.anonymous = false;
        
    };

    // Get User email
    User.prototype.getSocialId = function() {
        return this.social_id;
    };

    // Get User first name
    User.prototype.getFirstName = function() {
        return this.firstName;
    };

    // Get User last name
    User.prototype.getLastName = function() {
        return this.lastName;
    };

    // Get User full name
    User.prototype.getFullName = function() {
        return this.getFirstName() + " " + this.getLastName();
    };

    // Is User authenticated?
    User.prototype.isAuthenticated = function() {
        return (this.readyForAuth || this.anonymous) && this.token != null;
    };

    // Set User Auth token
    User.prototype.setAuthToken = function(token) {
        //
    };

    // Get User Auth token
    User.prototype.getAuthToken = function() {
        return this.token;
    };

    User.prototype.signIn = function() {
        this.readyForAuth = true;
        hello('google').login();
    };

    User.prototype.signOut = function() {
        localStorage.removeItem(ls_record_name);
        localStorage.removeItem(token_record_name);

        globalService.clearSubscriptions();

        this.social_id = null;
        this.first_name = null;
        this.last_name = null;
        this.token = null;
        this.readyForAuth = false;
        this.setUser(this);

        $rootScope.user = new User(this);
        console.log('user signed out');
        $location.path('');
    }

    User.prototype.getProfile = function(first_name, last_name, social_id) {
        var self = this;
        var bcrypt = new bCrypt();

        bcrypt.hashpw(first_name+"!@#$%^&*"+last_name, $secret, function(password_hash) {

            rest_services_url = $rootScope.rest_url;
            var service_url = rest_services_url + general_service_endpoint;
            var payload = JSON.stringify({
                "first_name": first_name, 
                "last_name": last_name, 
                "social_id": social_id, 
                "password": password_hash
            });

            $http({
                url     :   service_url,
                method  :   'POST',
                data    :   payload,
                headers :   {
                    'Content-Type': 'application/json'
                }
            }).success(function(response) {

                // prepare user account
                self.firstName = first_name;
                self.lastName = last_name;
                self.social_id = social_id;
                self.token = response.token;
                self.setUser(self);

                localStorage.removeItem(ls_record_name);
                localStorage.setItem(ls_record_name, JSON.stringify(self));

                localStorage.removeItem(token_record_name);
                localStorage.setItem(token_record_name, response.token);

                // setup websockets && message source_id
                if($rootScope.wsDispatcher == undefined) {
                    $rootScope.wsDispatcher = globalService.initWsDispatcher();
                    $rootScope.source_id = globalService.getRandString(50);
                }

                // update token when new session for same user started
                var subscriptionData = {
                    channel     :    'users',
                    event       :    'refreshed_token'
                };
                if(!globalService.isSubscriptionBinded(subscriptionData)) {
                    globalService.bindSubscription(subscriptionData);

                    $rootScope.wsDispatcher.subscribe('users').bind('refreshed_token', function(message) { 
                        if(self.social_id != message.user) 
                            return;

                        self.token = message.token;
                        localStorage.removeItem(token_record_name);
                        localStorage.setItem(token_record_name, message.token);
                    });
                }

                console.log('user signed in');

                $rootScope.loading();
            });
        }, function() {});     
    };

    return User;
});

/*
* Controllers
*/
// Users management controller
usersModule.controller('usersController', function() {
   //

});
