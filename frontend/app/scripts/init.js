'use strict'
var app_modules = {
    angular             :   ['ngAnimate','ngAria','ngCookies','ngMessages','ngResource','ngRoute','ngSanitize','ngTouch'],
    extensions          :   ['ag.directives'],
    iosrApp             :   ['coreModule', 'usersModule', 'spreadsheetsModule', 'chartsModule']
}

var $initialized = false;

/**
 * @ngdoc overview
 * @name iosrApp
 * @description
 * # iosrApp
 *
 * Main module of the application.
 */
var iosrApp = angular.module('iosrApp', [].concat(app_modules.angular, app_modules.extensions, app_modules.iosrApp));

iosrApp.config(function ($routeProvider, $locationProvider, $httpProvider) {

  $locationProvider.html5Mode(true).hashPrefix('!');

  $routeProvider
    // route for the home page
    .when('\/:componentName?\/:action1?\/:action2?\/:action3?\/:action4?\/', {
        templateUrl : 'views/init.html',
        controller  : 'initCtrl'
        //templateUrl : 'views/charts.html',
        //controller  : 'chartsController'
    })
    .otherwise({
      redirectTo: "/"
    });

  //initialize get if not there
  if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
  }
  //disable ajax request caching
  $httpProvider.defaults.headers.get['If-Modified-Since'] = '0';
  $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
  $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';

  // Request & response default types
  $httpProvider.defaults.responseType = "application/hal+json";
  $httpProvider.defaults.headers.common = {
    "Accept"                        :   "application/hal+json, application/json, text/plain, */*",
    "Content-Type"                  :   'application/json'
  };

});

iosrApp.controller('initCtrl', function($scope, $rootScope, $route, $routeParams, $location, User, Component, globalService) {
  $scope.initialized = $initialized;
  $rootScope.rest_url = $rest_url;
  $rootScope.errorOccurred = false; 

  // Create User object & bind it to root scope
  if(!$rootScope.user || $rootScope.user.anonymous) {
    if(localStorage.getItem("iosrApp-user") && JSON.parse(localStorage.getItem("iosrApp-user")).anonymous && !localStorage.getItem("anonymousAttempt"))
      localStorage.removeItem("iosrApp-user"); // if anonymous user still signed in, but app didn't ask for it - forget about him
    localStorage.removeItem("anonymousAttempt"); // forget about anonymous user before next login request
    $rootScope.user = new User(JSON.parse(localStorage.getItem("iosrApp-user")));
  }

  // Config oAuth app credentials
  hello.init(
    { 
      google : $google_client_id
    },
    {
      redirect_uri: $oauth_redirect_uri 
    }
  );

  // if anonymous user wants to access spreadsheet - let him try
  if($rootScope.user.social == null) {
    if($routeParams.componentName == 'spreadsheet' && !$scope.initialized) {
      localStorage.setItem("anonymousAttempt", true); // ask for anonymous user login
      $rootScope.user.getProfile('Anonymous', 'User', '9876543210');
    }
  }

  // When signed in with oAuth - log into application
  hello.on('auth.login', function(authenticator) {
    hello(authenticator.network).api( '/me' ).then(function(response) {
        if(localStorage.getItem("anonymousAttempt") || ($rootScope.user && $rootScope.user.anonymous))
          return; // prevent fetching oauth credentials when processing anonymous user request
        $rootScope.user.getProfile(response.first_name, response.last_name, response.id);
    });
  });

  // Scope loading
  $rootScope.loading = $scope.loading = function() {
    $rootScope.initialized = $initialized = true;
    $route.reload();
  };

  // Resolve route & setup component url to load
  if($scope.initialized) {
    if("componentName" in $routeParams) {
      $rootScope.component = new Component({
        path: $routeParams.componentName,
        route: $routeParams,
      });
    }
    else {
      $rootScope.component = new Component({
        path            :   "spreadsheet",
        route           :   $routeParams
      });
    }
  }

});
