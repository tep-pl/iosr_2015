'use strict'
/**
* 	Spreadsheets IOSR Configuration File
**/

// Remote Service URL
var $rest_url = "http://127.0.0.1:3000/";
var $websocket_url = "localhost:3000/websocket";

/**
* oAuth credentials
**/
// dev
var $google_client_id = "799180496833-k8vui17mh4gpie61q43pkufksf06agr9.apps.googleusercontent.com";
var $oauth_redirect_uri = "http://localhost:9000";

// Salt for user's password generation
var $secret = "$2a$05$dP9aMkS02wQYKYfDeeHxS."

