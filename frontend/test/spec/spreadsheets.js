describe('spreadsheet model instantiation', function () {
  beforeEach(module('iosrApp'));
  beforeEach(module('spreadsheetsModule'));
  var spreadsheet, scope, globalService;

  beforeEach(function () {
   globalService = {};
   globalService.getRequestHeaders = function(token) {
    return 'X-Auth-Token: ' + token;
   };

   module(function ($provide) {
    $provide.value('globalService', globalService);
   });

  });

  beforeEach(inject(function($rootScope, $httpBackend, Spreadsheet) {

   scope = $rootScope.$new();
   scope.rest_url = 'http://localhost:3000/';
   scope.user = { };
   scope.user.getAuthToken = function() { return 'testToken' };

   $httpBackend.when('POST', 'http://localhost:3000/api/spreadsheets').respond({id: 1, title: "test", shared: false, cellsDictionary: {}});
   spreadsheet = new Spreadsheet(null);
  }));

  it('should correctly prepare empty spreadsheet instance', function () {
    expect(spreadsheet).not.toBe(undefined);
    expect(spreadsheet.id).toBe("");
    expect(spreadsheet.title).toBe("");
    expect(spreadsheet.shared).toBe(false);
    expect(spreadsheet.cellsDictionary).toEqual({});
  });


});