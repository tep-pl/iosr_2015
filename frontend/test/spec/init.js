'use strict';

describe('initCtrl when no one logged in', function () {
  beforeEach(module('iosrApp'));
  var initCtrl, scope;
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    initCtrl = $controller('initCtrl', {
      $scope: scope
    }); 
  }));

  it('should properly initialize scope variables when app just started', function () {
    expect(scope.errorOccurred).not.toBe(undefined);
    expect(scope.rest_url).not.toBe(undefined);
    expect(scope.initialized).not.toBe(undefined);
    expect(scope.initialized).toBe(false);
  });

  it('should properly initialize empty user account when app just started', function() {
    expect(scope.user).not.toBe(undefined);
    expect(scope.user.social_id).toBe(null);
    expect(scope.user.token).toBe(null);
    expect(scope.user.readyForAuth).toBe(false);
  });
});

describe('initCtrl when anonymous attempt performed', function () {
  beforeEach(module('iosrApp'));
  var initCtrl, scope, window;
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    var store = {
      "anonymousAttempt": true,
      "iosrApp-user": JSON.stringify({
        firstName: 'Anonymous',
        lastName: 'User',
        social_id: '9876543210',
        token: 'testToken',
        anonymous: true
      })
    };
    inject(function($window) {
      window = $window;
    });
    spyOn(window.localStorage, 'getItem').and.callFake(function (key) {
      return store[key];
    });
    spyOn(window.localStorage, 'setItem').and.callFake(function (key, value) {
      return store[key] = value + '';
    });
    spyOn(window.localStorage, 'removeItem').and.callFake(function (key, value) {
      delete store[key];
    });
    initCtrl = $controller('initCtrl', {
      $scope: scope
    }); 
  }));

  it('should properly load anonymous user account when asked and clear attempt info', function() {
    expect(scope.user.social_id).toBe('9876543210');
    expect(window.localStorage.getItem('anonymousAttempt')).toBe(undefined);
  });
});

describe('initCtrl when anonymous attempt performed before but not now', function () {
  beforeEach(module('iosrApp'));
  var initCtrl, scope, window;
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    var store = {
      "anonymousAttempt": false,
      "iosrApp-user": JSON.stringify({
        firstName: 'Anonymous',
        lastName: 'User',
        social_id: '9876543210',
        token: 'testToken',
        anonymous: true
      })
    };
    inject(function($window) {
      window = $window;
    });
    spyOn(window.localStorage, 'getItem').and.callFake(function (key) {
      return store[key];
    });
    spyOn(window.localStorage, 'setItem').and.callFake(function (key, value) {
      return store[key] = value + '';
    });
    spyOn(window.localStorage, 'removeItem').and.callFake(function (key, value) {
      store[key] = null;
    });
    initCtrl = $controller('initCtrl', {
      $scope: scope
    }); 
  }));

  it('should properly clear anonymous user info if any was signed in before', function() {
    expect(window.localStorage.getItem('anonymousAttempt')).toBe(null);
    expect(window.localStorage.getItem('iosrApp-user')).toBe(null);
  });
});

describe('initCtrl when real user properly signed in', function () {
  beforeEach(module('iosrApp'));
  var initCtrl, scope, window;
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    var store = {
      "anonymousAttempt": false,
      "iosrApp-user": JSON.stringify({
        social_id: '123456789',
        firstName: 'Jan',
        lastName: 'Kowalski',
        token: 'testTokenNonAnonymous',
        readyForAuth: false,
        anonymous: false
      })
    };
    inject(function($window) {
      window = $window;
    });
    spyOn(window.localStorage, 'getItem').and.callFake(function (key) {
      return store[key];
    });
    spyOn(window.localStorage, 'setItem').and.callFake(function (key, value) {
      return store[key] = value + '';
    });
    initCtrl = $controller('initCtrl', {
      $scope: scope
    }); 
  }));

  it('should properly sign typical user in', function() {
    expect(scope.user).not.toBe(undefined);
    expect(scope.user.firstName).toBe('Jan');
    expect(scope.user.lastName).toBe('Kowalski');
    expect(scope.user.social_id).toBe('123456789');
  });
});

