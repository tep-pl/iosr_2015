describe('user model instantiation', function () {
  beforeEach(module('iosrApp'));
  beforeEach(module('usersModule'));
  var user;

  it('should correctly prepare empty user instance', function () {

    inject(function (User) {
      user = new User(null);
    });

    expect(user).not.toBe(undefined);
    expect(user.firstName).toBe(null);
    expect(user.lastName).toBe(null);
    expect(user.social_id).toBe(null);
    expect(user.token).toBe(null);
    expect(user.readyForAuth).toBe(false);
    expect(user.anonymous).toBe(false);
  });

  it('should correctly prepare anonymous user instance', function () {

    inject(function (User) {
      user = new User({firstName: 'Anonymous', lastName: 'User'});
    });

    expect(user).not.toBe(undefined);
    expect(user.firstName).toBe('Anonymous');
    expect(user.lastName).toBe('User');
    expect(user.social_id).toBe(null);
    expect(user.token).toBe(null);
    expect(user.readyForAuth).toBe(false);
    expect(user.anonymous).toBe(true);
  });

  it('should correctly see user as authenticated when really is', function () {

    inject(function (User) {
      user = new User({firstName: 'Test', lastName: 'Test', readyForAuth: true, token: 'testTOKEN'});
    });

    expect(user).not.toBe(undefined);
    expect(user.firstName).toBe('Test');
    expect(user.lastName).toBe('Test');
    expect(user.social_id).toBe(null);
    expect(user.token).not.toBe(null);
    expect(user.readyForAuth).toBe(true);
    expect(user.anonymous).toBe(false);
    expect(user.isAuthenticated()).toBe(true);
  });

  it('should not see user as authenticated when not asked for that', function () {

    inject(function (User) {
      user = new User({firstName: 'Test', lastName: 'Test', readyForAuth: false, token: 'testTOKEN'});
    });

    expect(user).not.toBe(undefined);
    expect(user.firstName).toBe('Test');
    expect(user.lastName).toBe('Test');
    expect(user.social_id).toBe(null);
    expect(user.token).not.toBe(null);
    expect(user.readyForAuth).toBe(false);
    expect(user.anonymous).toBe(false);
    expect(user.isAuthenticated()).toBe(false);
  });

  it('should see user as authenticated if anonymous and backend app token received', function () {

    inject(function (User) {
      user = new User({firstName: 'Anonymous', lastName: 'User', readyForAuth: false, token: 'testTOKEN'});
    });

    expect(user).not.toBe(undefined);
    expect(user.firstName).toBe('Anonymous');
    expect(user.lastName).toBe('User');
    expect(user.social_id).toBe(null);
    expect(user.token).not.toBe(null);
    expect(user.readyForAuth).toBe(false);
    expect(user.anonymous).toBe(true);
    expect(user.isAuthenticated()).toBe(true);
  });

});